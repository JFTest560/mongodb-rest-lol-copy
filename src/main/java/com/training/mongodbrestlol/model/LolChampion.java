package com.training.mongodbrestlol.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter // lombok to skip defining getters/setters
@Setter
@Document // mongoDB Document object
public class LolChampion {

    @Id // mongoDB Document id
    private String id;
    private String name;
    private String role;
    private String difficulty;

    @Override
    public String toString() {
        return "LolChampion{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", role='" + role + '\'' +
                ", difficulty='" + difficulty + '\'' +
                '}';
    }

}
