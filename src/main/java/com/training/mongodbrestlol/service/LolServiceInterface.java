package com.training.mongodbrestlol.service;

import com.training.mongodbrestlol.model.LolChampion;

import java.util.List;
import java.util.Optional;

public interface LolServiceInterface {

    List<LolChampion> findAll();
    LolChampion save(LolChampion lolChampion);
    Optional<LolChampion> findById(String id);
    void delete(String id);

}
