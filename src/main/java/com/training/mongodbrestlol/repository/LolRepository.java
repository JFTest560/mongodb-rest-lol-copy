package com.training.mongodbrestlol.repository;

import com.training.mongodbrestlol.model.LolChampion;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component // repository/data access bean object
public interface LolRepository extends MongoRepository<LolChampion, String> {
}
